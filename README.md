Welcome to JTalker!
=
**JTalker** is a Project we started about a year ago out of boredom. It is a **Lanchat** implemented in **Java** and is based on a classic server/client model.
It's not used anywhere regularly and it's just a fun project we work on in our spare time. We're mostly working alone on this Project but we will accept pull requests if they fit in the style of the project and have well written commit messages! (See _Commit naming_)

---

Getting started
-

For setting up a version controll, cloning this repository and much more, you can look into the [Bitbucket Cloud Documentation](https://confluence.atlassian.com/bitbucket/bitbucket-cloud-documentation-home-221448814.html).

---

Commit naming
-

Commit naming is very important to maintain a consistent project. Because of this we won't accept commits, which aren't named after these 7 guidelines.
>**Commit naming**  
> - Separate subject from body with a blank line  
> - Limit the subject line to 50 characters  
> - Capitalize the subject line  
> - Do not end the subject line with a period  
> - Use the imperative mood in the subject line  
> - Wrap the body at 72 characters  
> - Use the body to explain *what* and *why* vs. *how*  

These guidelines are taken from a blog post, which you can check out [here](http://chris.beams.io/posts/git-commit/).

---