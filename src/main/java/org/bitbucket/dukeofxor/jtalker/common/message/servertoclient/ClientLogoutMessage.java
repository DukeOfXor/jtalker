package main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient;

import java.io.Serializable;

import main.java.org.bitbucket.dukeofxor.jtalker.client.LogoutState;

public class ClientLogoutMessage implements Serializable{
	
	private static final long serialVersionUID = -696909159415060009L;
	private String message;
	private LogoutState logoutState;

	public ClientLogoutMessage(String message, LogoutState logoutState) {
		setMessage(message);
		setLogoutState(logoutState);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LogoutState getLogoutState() {
		return logoutState;
	}

	public void setLogoutState(LogoutState logoutState) {
		this.logoutState = logoutState;
	}
}
