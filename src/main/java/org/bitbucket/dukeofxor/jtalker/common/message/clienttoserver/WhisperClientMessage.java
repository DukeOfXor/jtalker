
package main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver;

import java.io.Serializable;

public class WhisperClientMessage implements Serializable {

  private static final long serialVersionUID = 2413706678071790117L;
  private String text;
  private String targetUsername;
  private String hostUsername;

  public WhisperClientMessage(String text, String targetUsername, String hostUserName) {
    setText(text);
    setTargetUsername(targetUsername);
    setHostUsername(hostUserName);
  }

  public String getMessage() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getTargetUsername() {
    return targetUsername;
  }

  public void setTargetUsername(String targetUsername) {
    this.targetUsername = targetUsername;
  }

  public String getHostUsername() {
    return hostUsername;
  }

  public void setHostUsername(String hostUsername) {
    this.hostUsername = hostUsername;
  }

}
