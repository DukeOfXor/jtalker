
package main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient;

import java.io.Serializable;

public class ErrorServerMessage implements Serializable {

  private static final long serialVersionUID = 7137427253849333346L;
  private String errorMessage;

  public ErrorServerMessage(String errorMessage) {
    setErrorMessage(errorMessage);
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
