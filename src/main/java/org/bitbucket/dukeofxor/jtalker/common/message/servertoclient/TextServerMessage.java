package main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient;

import java.io.Serializable;

public class TextServerMessage implements Serializable {

	private static final long serialVersionUID = -4528092934828619618L;
	private String username;
	private String text;

	/**
	 * @param username
	 * @param text
	 */
	public TextServerMessage(String username, String text) {
		this.setUsername(username);
		this.setText(text);
	}

	/**
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 */
	private void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 */
	private void setText(String text) {
		this.text = text;
	}
}
