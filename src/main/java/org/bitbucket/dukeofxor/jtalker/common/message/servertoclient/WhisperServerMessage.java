
package main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient;

import java.io.Serializable;

public class WhisperServerMessage implements Serializable {

  private static final long serialVersionUID = -7259455925880407951L;
  private String targetUsername;
  private String hostUsername;
  private String text;

  /**
   * @param text
   * @param targetUsername
   * @param hostUsername
   */
  public WhisperServerMessage(String text, String targetUsername, String hostUsername) {
    setText(text);
    setTargetUsername(targetUsername);
    setHostUsername(hostUsername);
  }

  public String getTargetUsername() {
    return targetUsername;
  }

  public void setTargetUsername(String username) {
    this.targetUsername = username;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getHostUsername() {
    return hostUsername;
  }

  public void setHostUsername(String hostUsername) {
    this.hostUsername = hostUsername;
  }

}
