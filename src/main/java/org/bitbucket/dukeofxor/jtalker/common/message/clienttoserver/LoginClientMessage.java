package main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver;

import java.io.Serializable;

public class LoginClientMessage implements Serializable {

	private static final long serialVersionUID = -5570709010741836768L;
	private String username;

	/**
	 * @param username
	 */
	public LoginClientMessage(String username) {
		setUsername(username);
	}

	/**
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 */
	private void setUsername(String username) {
		this.username = username;
	}
}
