package main.java.org.bitbucket.dukeofxor.jtalker.server.gui;


import java.net.Inet4Address;
import java.net.UnknownHostException;

import javafx.application.Application;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import main.java.org.bitbucket.dukeofxor.jtalker.server.ClientThread;
import main.java.org.bitbucket.dukeofxor.jtalker.server.Server;
import main.java.org.bitbucket.dukeofxor.jtalker.server.log.ServerLogger;
import main.java.org.bitbucket.dukeofxor.jtalker.server.log.ServerMessageType;

public class ServerUI extends Application implements IServerUI{

	private static final String START_COMMAND = "start";
	private static final String STOP_COMMAND = "stop";
	private GridPane panelInfo;
	private VBox toolBar;
	
	private Server jTalkerServer;
	
	private ListView<ClientThread> listViewClients;
	private ObservableList<ClientThread> connectedClients;
	private ListProperty<ClientThread> listPropertyClients;
	private FilteredList<ClientThread> filteredClientList;
	private CommandHandler commandHandler;
	private SplitPane rootPane;
	private TextArea textAreaLog;
	private Label labelInfo;
	private BorderPane panelWrapperCenter;
	private Label labelIp;
	private GridPane panelWrapperToolBar;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		setupEnviroment();
		rootPane = new SplitPane();
		rootPane.getStyleClass().add("rootPaneServer");
		
		panelWrapperCenter = new BorderPane();
		
		createToolbar();
		
		createClientList();
		
		createServerNotRunningContent();
		
		createServerRunningContent();
		
		updateState();
		
		rootPane.getItems().addAll(panelWrapperToolBar, panelWrapperCenter);
		
		BorderPane panelWrapperTop = new BorderPane();
		panelWrapperTop.setCenter(labelIp);
		
		panelWrapperCenter.setTop(panelWrapperTop);
		panelWrapperCenter.getStyleClass().add("contentBackground");
		
		Scene scene = new Scene(rootPane, 500, 150);
		setSplitPaneDivider();
		scene.getStylesheets().add(getClass()
				.getResource("/main/resources/org/bitbucket/dukeofxor/jtalker/gui/styles.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				if(getJTalkerServer().isRunning()){
					getJTalkerServer().shutdown();
				}
			}
		});
		
		primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
			
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				setSplitPaneDivider();
				labelIp.setStyle("-fx-font-size: " + primaryStage.getWidth() / 30);
				labelIp.setId("labelIp");
				
			}
		});
		
		primaryStage.show();
	}

	private void createServerRunningContent() throws UnknownHostException {
		textAreaLog = new TextArea();
		textAreaLog.setEditable(false);
		textAreaLog.setWrapText(true);
		textAreaLog.getStyleClass().add("text-area-log");
		
		labelIp = new Label(Inet4Address.getLocalHost().getHostAddress() + ":" + Server.PORT);
		labelIp.setId("labelIp");
		Tooltip labelIpTooltip = new Tooltip("Click to copy the address");
		Tooltip.install(labelIp, labelIpTooltip);
		labelIp.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				Clipboard clipboard = Clipboard.getSystemClipboard();
				ClipboardContent content = new ClipboardContent();
				content.putString(labelIp.getText());
				clipboard.setContent(content);
			}
		});
	}

	private void createServerNotRunningContent() {
		panelInfo = new GridPane();
		labelInfo = new Label("Server currently not running");
		labelInfo.setId("labelInfo");
		panelInfo.add(labelInfo, 0, 0);
		panelInfo.getStyleClass().add("contentBackground");
		panelInfo.setAlignment(Pos.CENTER);
	}

	private void createClientList() {
		connectedClients = FXCollections.observableArrayList();
		listPropertyClients = new SimpleListProperty<ClientThread>();
		filteredClientList = new FilteredList<ClientThread>(connectedClients, s -> true);
		
		listViewClients = new ListView<ClientThread>();
		listViewClients.getStyleClass().add("list-view-server");
		listViewClients.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		listViewClients.setCellFactory(new Callback<ListView<ClientThread>, ListCell<ClientThread>>() {

			@Override
			public ListCell<ClientThread> call(ListView<ClientThread> param) {
				ListCell<ClientThread> cell = new ListCell<ClientThread>() {
					@Override
					protected void updateItem(ClientThread clientThread, boolean empty) {
						super.updateItem(clientThread, empty);
						if(clientThread == null || empty){
							setText(null);
						} else{
							setText(clientThread.getUsername());
						}
					}

				};
				return cell;
			}
		});		
		
		listPropertyClients.set(connectedClients);
		listViewClients.itemsProperty().bindBidirectional(listPropertyClients);
	}

	private void setupEnviroment() {
		jTalkerServer = new Server(this);
		commandHandler = new CommandHandler(this, jTalkerServer);
	}

	private void createToolbar() throws UnknownHostException {
		panelWrapperToolBar = new GridPane();
		toolBar = new VBox();
		toolBar.setId("toolBar");
		
		//Start
		Label labelState = new Label();
		labelState.setId("labelStart");
		labelState.getStyleClass().add("hoverHand");
		labelState.setOnMouseClicked(new EventHandler<MouseEvent>() {
			
			@Override
			public void handle(MouseEvent event) {
				updateState();
				if(getJTalkerServer().isRunning()){
					labelState.setId("labelStart");
					commandHandler.handleCommand(STOP_COMMAND);
				}else {
					labelState.setId("labelStop");
					commandHandler.handleCommand(START_COMMAND);
				}
			}
		});
		
		//Console
		Label labelConsole = new Label();
		labelConsole.setId("labelConsole");
		labelConsole.getStyleClass().add("hoverHand");
		labelConsole.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				panelWrapperCenter.setCenter(textAreaLog);
			}
		});
		
		//Clients
		Label labelClients = new Label();
		labelClients.setId("labelClients");
		labelClients.getStyleClass().add("hoverHand");
		labelClients.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				panelWrapperCenter.setCenter(listViewClients);
			}
		});
		
		toolBar.getChildren().addAll(labelState, labelConsole, labelClients);
		toolBar.setSpacing(20);
		panelWrapperToolBar.add(toolBar, 0, 0);
		panelWrapperToolBar.setAlignment(Pos.CENTER);
		
	}

	public void addClientThread(ClientThread clientThread) {
		connectedClients.add(clientThread);
		listViewClients.setItems(filteredClientList);
	}

	public void removeClientThread(ClientThread clientThread) {
		connectedClients.remove(clientThread);
		listViewClients.setItems(filteredClientList);
	}

	public void displayMessage(ServerMessageType logType, String message) {
		ServerLogger.getInstance().log(logType, message);
		
		textAreaLog.appendText(message);
		textAreaLog.appendText("\n");
	}

	public void updateState() {
		if (getJTalkerServer().isRunning()) {
			styleRunGUI();
			panelWrapperCenter.setCenter(textAreaLog);
		} else {
			styleStopGUI();
			panelWrapperCenter.setCenter(panelInfo);
		}		
	}

	private void styleStopGUI() {
		rootPane.setId("serverStop");
	}

	private void styleRunGUI() {
		rootPane.setId("serverRun");
	}

	public Server getJTalkerServer() {
		return this.jTalkerServer;
	}

	public void setJTalkerServer(Server server) {
		this.jTalkerServer = server;
	}

	public void clearConsole() {
		textAreaLog.clear();
	}

	public void close() {
		getJTalkerServer().shutdown();
		System.exit(0);
	}
	
	private void setSplitPaneDivider(){
		rootPane.setDividerPosition(0, 0.1f);
	}

}
