
package main.java.org.bitbucket.dukeofxor.jtalker.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import main.java.org.bitbucket.dukeofxor.jtalker.client.LogoutState;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver.LoginClientMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver.LogoutClientMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver.TextClientMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver.WhisperClientMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.ClientLogoutMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.ErrorServerMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.TextServerMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.WhisperServerMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.server.gui.IServerUI;
import main.java.org.bitbucket.dukeofxor.jtalker.server.log.ServerLogger;
import main.java.org.bitbucket.dukeofxor.jtalker.server.log.ServerMessageType;

public class ClientThread extends Thread {

	private Socket socket;
	private ObjectInputStream inputStream;
	private ObjectOutputStream outputStream;
	private Server server;
	private String username;
	private boolean isLoggedIn;
	private Object receivedObject;

	private static final String USER_NOT_ONLINE = "User not online";
	private static final String WHISPER_TO_YOURSELF = "You cant whisper to yourself";

	/**
	 * @param socket
	 * @param gui
	 * @param server
	 */
	public ClientThread(Socket socket, IServerUI gui, Server server) {
		this.socket = socket;
		this.server = server;

		try {
			outputStream = new ObjectOutputStream(socket.getOutputStream());
			inputStream = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		log("<Connected>");
	}

	public void run() {
		boolean running = true;
		while (running) {
			try {
				receivedObject = inputStream.readObject();
			} catch (ClassNotFoundException e) {
				server.removeClient(this);
				shutdown();
				break;
			} catch (IOException e) {
				// this will throw if a client disconnects
				log("<Disconnected without logging out>");
				server.removeClient(this);
				shutdown();
				break;
			}

			// The following messages from the client get handled, even if the
			// client is not logged in

			// LoginMessage
			if (receivedObject.getClass().equals(LoginClientMessage.class)) {
				LoginClientMessage loginMessage = (LoginClientMessage) receivedObject;
				
				if(server.getClientByName(loginMessage.getUsername()) == null){
					this.username = loginMessage.getUsername();
					isLoggedIn = true;
					server.addClient(this);
					log("<Logged in>");
					continue;
				} else{
					//duplicated username
					server.sendObject(this, new ClientLogoutMessage(loginMessage.getUsername(), LogoutState.DUPLICATE_USERNAME));
				}

			}

			// The following messages from the client only get handled if the
			// client is logged in
			// If a not logged in client sends such a message, he will be
			// disconnected
			if (isLoggedIn) {
				// LogoutMessage
				if (receivedObject.getClass().equals(LogoutClientMessage.class)) {
					isLoggedIn = false;
					server.removeClient(this);
					log("<Logged out>");
					running = false;
					log("<Disconnected>");
					continue;
				}

				// TextMessage
				if (receivedObject.getClass().equals(TextClientMessage.class)) {
					TextClientMessage textMessage = (TextClientMessage) receivedObject;
					server.broadcast(new TextServerMessage(username, textMessage.getText()));
					log(textMessage.getText());
				}

				// WhisperMessage
				if (receivedObject.getClass().equals(WhisperClientMessage.class)) {
					WhisperClientMessage whisperMessage = (WhisperClientMessage) receivedObject;
					
					ClientThread hostClient = server.getClientByName(whisperMessage.getHostUsername());

					if (server.getClientByName(whisperMessage.getTargetUsername()) != null) {
						
						
						//Check if not whisper to yourself
						if(!whisperMessage.getHostUsername().equals(whisperMessage.getTargetUsername())){
							ClientThread targetClient = server.getClientByName(whisperMessage.getTargetUsername());
							
							WhisperServerMessage whisperClientMessage = new WhisperServerMessage(whisperMessage.getMessage(), targetClient.getUsername(), hostClient.getUsername());
							
							server.sendObject(targetClient, whisperClientMessage);
							server.sendObject(hostClient, whisperClientMessage);
							
						} else{
							server.sendObject(hostClient, new ErrorServerMessage(WHISPER_TO_YOURSELF));
						}
						
						
					} else {
						server.sendObject(hostClient,
								new ErrorServerMessage(USER_NOT_ONLINE));
					}
				}

			} else {
				running = false;
				log("<Disconnected>");
				continue;
			}
		}
		server.removeClient(this);
		shutdown();
	}

	public void shutdown() {
		try {
			if (inputStream != null) {
				inputStream.close();
			}
			if (outputStream != null) {
				outputStream.close();
			}
			if (socket != null) {
				socket.close();
			}
		} catch (IOException e) {
		}
	}

	private void log(String message) {
		if (getUsername() == null) {
			ServerLogger.getInstance().log(ServerMessageType.CLIENT, "[" + getIp() + "] " + message);
		} else {
			ServerLogger.getInstance().log(ServerMessageType.CLIENT,
					"[" + getIp() + ", " + getUsername() + "] " + message);
		}
	}

	/**
	 * Check if this client has a connection to the server
	 */
	public boolean isConnected() {
		if (socket.isConnected()) {
			return true;
		} else {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	/**
	 * Write a message to this client
	 * 
	 * @param message
	 */
	public void writeObject(Object message) {
		try {
			outputStream.writeObject(message);
		} catch (IOException e) {
			log("<Error sending message>");
		}
	}

	/**
	 * Check if this client is logged in
	 */
	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	public String getUsername() {
		return username;
	}

	public String getIp() {
		return socket.getInetAddress().getHostAddress();
	}

	public void kickClient(String reason) {
		writeObject(new ClientLogoutMessage(reason, LogoutState.KICK));
		server.removeClient(this);
		shutdown();
	}

}
