
package main.java.org.bitbucket.dukeofxor.jtalker.server;

import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.ClientListServerMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.server.gui.IServerUI;
import main.java.org.bitbucket.dukeofxor.jtalker.server.log.ServerLogger;
import main.java.org.bitbucket.dukeofxor.jtalker.server.log.ServerMessageType;

public class Server extends Thread {

	private Boolean running = false;
	private IServerUI gui;
	private ObservableList<ClientThread> connectedClients = FXCollections.observableArrayList();
	private ServerSocket serverSocket;

	public static final int PORT = 8954;

	/**
	 * @param gui
	 */
	public Server(IServerUI gui) {
		this.gui = gui;

		// Sending a ClientList update to all Clients when a Client
		// Connects/Disconnects
		connectedClients.addListener(new ListChangeListener<ClientThread>() {
			@Override
			public void onChanged(javafx.collections.ListChangeListener.Change<? extends ClientThread> c) {
				if (running) {
					ArrayList<String> usernames = new ArrayList<>();
					for (ClientThread clientThread : connectedClients) {
						usernames.add(clientThread.getUsername());
					}
					broadcast(new ClientListServerMessage(usernames));
				}
			}
		});
	}

	@Override
	public void run() {
		try {
			serverSocket = new ServerSocket(PORT);

			running = true;
			changeGuiState();

			ServerLogger.destroyInstance();
			displayGuiMessage(ServerMessageType.INFO,
					"Logfile created at " + ServerLogger.getInstance().getLogFilePath());
			displayGuiMessage(ServerMessageType.INFO, "Server started");
			while (running) {
				// server loop
				Socket socket = serverSocket.accept();
				if (!running) {
					break;
				}
				ClientThread clientThread = new ClientThread(socket, gui, this);
				clientThread.start();
			}
		} catch (SocketException e1) {
			running = false;
			if (e1.getMessage().equals("socket closed")) {
				// serverSocket.accept() will throw this exception because the
				// socket gets closed, while its waiting for connections
				// this can't be prevented, therefore the exception is ignored
			} else if (e1.getMessage().equals("Address already in use: JVM_Bind")) {
				// an exception with this message gets thrown if the address is
				// already in use, i.e. when there is already a server running
				// on this pc
				displayGuiMessage(ServerMessageType.ERROR,
						"Can't start server. Maybe there is already a server running on this computer?");
			} else {
				e1.printStackTrace();
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	/**
	 * Send a message to all Clients
	 * 
	 * @param message
	 */
	synchronized void broadcast(Object message) {
		for (ClientThread clientThread : getConnectedClients()) {
			clientThread.writeObject(message);
		}
	}

	/**
	 * @param client
	 * @param objectToSend
	 */
	public void sendObject(ClientThread client, Serializable objectToSend) {
		client.writeObject(objectToSend);
	}

	/**
	 * @param clientThreadToAdd
	 */
	synchronized void addClient(ClientThread clientThreadToAdd) {
		getConnectedClients().add(clientThreadToAdd);
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				gui.addClientThread(clientThreadToAdd);
			}
		});
	}

	/**
	 * @param clientThreadToRemove
	 */
	synchronized void removeClient(ClientThread clientThreadToRemove) {
		getConnectedClients().remove(clientThreadToRemove);
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				gui.removeClientThread(clientThreadToRemove);
			}
		});
	}

	public void shutdown() {
		try {
			if (serverSocket != null) {
				serverSocket.close();
			}
			for (ClientThread clientThread : getConnectedClients()) {
				clientThread.shutdown();
			}
			running = false;
			changeGuiState();
			displayGuiMessage(ServerMessageType.INFO, "Server stopped");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Boolean isRunning() {
		return running;
	}

	/**
	 * Change the GUIs Running/Stopped status
	 */
	private void changeGuiState() {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				gui.updateState();
			}
		});

	}

	/**
	 * Display a message with the default prefix
	 * 
	 * @param message
	 */
	private void displayGuiMessage(ServerMessageType logType, String message) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				gui.displayMessage(logType, message);
			}
		});
	}

	public ObservableList<ClientThread> getConnectedClients() {
		return connectedClients;
	}

	public ClientThread getClientByName(String clientName) {
		ObservableList<ClientThread> connectedClients = gui.getJTalkerServer().getConnectedClients();
		for (ClientThread clientThread : connectedClients) {
			if (clientThread.getUsername().equals(clientName)) {
				return clientThread;
			}
		}
		return null;
	}

	/**
	 * Kick client from server
	 * 
	 * @param clientThreadToKick
	 */
	public void kickClient(ClientThread clientThreadToKick, String reason) {
		clientThreadToKick.kickClient(reason);
	}
}
