
package main.java.org.bitbucket.dukeofxor.jtalker.server.log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerLogger {

  private static ServerLogger instance = null;
  private Path logFile;
  private String workingDir;
  private String fileSeparator;
  private String lineSeparator;
  private String logDir;
  private String logFileString;

  protected ServerLogger() {
    workingDir = System.getProperty("user.dir");
    fileSeparator = System.getProperty("file.separator");
    lineSeparator = System.getProperty("line.separator");

    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    String timestamp = sdf.format(date);

    logDir = workingDir + fileSeparator + "jtalker-log";
    logFileString = logDir + fileSeparator + "jtalker-server-" + timestamp + ".html";

    logFile = Paths.get(logFileString);

    String logfileCreatedMessage = "#Logfile created at " + timestamp + lineSeparator + "\n<table>";
    String style = "<style>table, th, td { border: 1px solid black; border-collapse: collapse; } .error{ background-color: rgba(255,0,0,0.5); } .warning{ background-color: orange; } .info{ background-color: rgba(0,0,255,0.5);}</style>";

    style += logfileCreatedMessage;

    try {
      File file = new File(logDir);
      file.mkdirs();
      Files.write(logFile, style.getBytes(), StandardOpenOption.CREATE_NEW);
    } catch (IOException e) {
    }
  }

  public static ServerLogger getInstance() {
    if (instance == null) {
      instance = new ServerLogger();
    }

    return instance;
  }

  public static void destroyInstance() {
    if (instance != null) {
      instance = null;
    }
  }

  /**
   * Write a log message to the logfile
   */
  public void log(ServerMessageType logType, String message) {
    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss");
    String timestamp = sdf.format(date);
    String messageToWrite = "<tr><td>" + timestamp + "</td><td>" + logType.toString() + "</td><td>" + message + "</td>";

    if (logType.equals(ServerMessageType.ERROR)) {
      messageToWrite = "<tr class=\"error\"><td>" + timestamp + "</td><td>" + logType.toString() + "</td><td>" + message + "</td>";
    }
    if (logType.equals(ServerMessageType.WARNING)) {
      messageToWrite = "<tr class=\"warning\"><td>" + timestamp + "</td><td>" + logType.toString() + "</td><td>" + message + "</td>";
    }
    if (logType.equals(ServerMessageType.INFO)) {
      messageToWrite = "<tr class=\"info\"><td>" + timestamp + "</td><td>" + logType.toString() + "</td><td>" + message + "</td>";
    }

    try {
      Files.write(logFile, messageToWrite.getBytes(), StandardOpenOption.APPEND);
    } catch (IOException e) {
    }
  }

  /**
   * Return the path of the logfile
   * 
   * @return logFilePath
   */
  public String getLogFilePath() {
    return logFileString;
  }
}