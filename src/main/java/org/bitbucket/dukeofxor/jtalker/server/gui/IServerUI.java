package main.java.org.bitbucket.dukeofxor.jtalker.server.gui;

import main.java.org.bitbucket.dukeofxor.jtalker.server.ClientThread;
import main.java.org.bitbucket.dukeofxor.jtalker.server.Server;
import main.java.org.bitbucket.dukeofxor.jtalker.server.log.ServerMessageType;

public interface IServerUI {

	public void addClientThread(ClientThread clientThread);
	
	public void removeClientThread(ClientThread clientThread);
	
	public void displayMessage(ServerMessageType logType, String message);
	
	public void updateState();
	
	public Server getJTalkerServer();
	
	public void setJTalkerServer(Server server);
	
	public void clearConsole();
	
	public void close();
	
}
