package main.java.org.bitbucket.dukeofxor.jtalker.server.log;

public enum ServerMessageType {
	ERROR, WARNING, INFO, CLIENT
}
