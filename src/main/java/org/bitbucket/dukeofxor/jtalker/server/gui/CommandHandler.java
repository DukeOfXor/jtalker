
package main.java.org.bitbucket.dukeofxor.jtalker.server.gui;

import javafx.collections.ObservableList;
import main.java.org.bitbucket.dukeofxor.jtalker.server.ClientThread;
import main.java.org.bitbucket.dukeofxor.jtalker.server.Server;
import main.java.org.bitbucket.dukeofxor.jtalker.server.log.ServerMessageType;

public class CommandHandler {

	public static final String TO_MANY_ARGUMENTS = "Too many arguments for command ";
	public static final String TOO_FEW_ARGUMENTS = "Too few arguments for command ";
	public static final String USAGE = "Usage: ";

	// syntax helps for each command
	public static final String START_USAGE = "start";
	public static final String STOP_USAGE = "stop";
	public static final String CLEAR_USAGE = "clear";
	public static final String EXIT_USAGE = "exit";
	public static final String LIST_USAGE = "list";
	public static final String KICK_USAGE = "kick [username] [Reason]";

	private IServerUI gui;
	private Server server;

	/**
	 * @param gui
	 */
	public CommandHandler(IServerUI gui, Server server) {
		this.gui = gui;
		this.server = server;
	}

	/**
	 * @param command
	 */
	public void handleCommand(String command) {
		String[] cmd = command.split("\\s+");
		if (cmd[0].equalsIgnoreCase("start")) {
			startServer(cmd);
			return;
		}
		if (cmd[0].equalsIgnoreCase("stop")) {
			stopServer(cmd);
			return;
		}
		if (cmd[0].equalsIgnoreCase("clear")) {
			clearConsole(cmd);
			return;
		}
		if (cmd[0].equalsIgnoreCase("exit")) {
			exit(cmd);
			return;
		}
		if (cmd[0].equalsIgnoreCase("list")) {
			clientList(cmd);
			return;
		}
		if (cmd[0].equalsIgnoreCase("kick")) {
			kickClient(cmd);
			return;
		}

		gui.displayMessage(ServerMessageType.ERROR, "Command not found");
	}

	private void clientList(String[] cmd) {
		if (cmd.length > 1) {
			gui.displayMessage(ServerMessageType.ERROR, TO_MANY_ARGUMENTS + cmd[0] + "\n" + USAGE + LIST_USAGE);
			return;
		}
		if (gui.getJTalkerServer().isRunning()) {
			ObservableList<ClientThread> connectedClients = gui.getJTalkerServer().getConnectedClients();
			StringBuilder output = new StringBuilder();
			for (ClientThread clientThread : connectedClients) {
				output.append(clientThread.getIp() + ", ");
				output.append(clientThread.getUsername());
				output.append("\n");
			}
			if (output.length() > 0) {
				output.insert(0, "Connected clients:\n");
				// remove last newline - not very beautiful, I know
				output.replace(output.length() - 1, output.length(), "");
			} else {
				output.append("No connected clients");
			}

			gui.displayMessage(ServerMessageType.INFO, output.toString());
		} else {
			gui.displayMessage(ServerMessageType.ERROR, "Please start the server before you execute this command");
		}
	}

	private void startServer(String[] cmd) {
		if (cmd.length > 1) {
			gui.displayMessage(ServerMessageType.ERROR, TO_MANY_ARGUMENTS + cmd[0] + "\n" + USAGE + START_USAGE);
			return;
		}
		// Start server
		if (gui.getJTalkerServer().isRunning()) {
			gui.displayMessage(ServerMessageType.ERROR, "Server is already running");
		} else {
			Server server = new Server(gui);
			server.start();
			gui.setJTalkerServer(server);
			gui.updateState();
		}

	}

	private void stopServer(String[] cmd) {
		if (cmd.length > 1) {
			gui.displayMessage(ServerMessageType.ERROR, TO_MANY_ARGUMENTS + cmd[0] + "\n" + USAGE + STOP_USAGE);
			return;
		}
		if (gui.getJTalkerServer().isRunning()) {
			gui.getJTalkerServer().shutdown();
		} else {
			gui.displayMessage(ServerMessageType.INFO, "Server not running");
		}
	}

	private void clearConsole(String[] cmd) {
		if (cmd.length > 1) {
			gui.displayMessage(ServerMessageType.ERROR, TO_MANY_ARGUMENTS + cmd[0] + "\n" + USAGE + CLEAR_USAGE);
			return;
		}
		gui.clearConsole();
	}

	private void exit(String[] cmd) {
		if (cmd.length > 1) {
			gui.displayMessage(ServerMessageType.ERROR, TO_MANY_ARGUMENTS + cmd[0] + "\n" + USAGE + EXIT_USAGE);
			return;
		}
		if (gui.getJTalkerServer().isRunning()) {
			gui.displayMessage(ServerMessageType.ERROR,
					"Can't exit while server is running. Please stop the server before executing this command");
			return;
		}
		gui.close();
	}

	private void kickClient(String[] cmd) {
		if (cmd.length < 3) {
			gui.displayMessage(ServerMessageType.ERROR, TOO_FEW_ARGUMENTS + cmd[0] + "\n" + USAGE + KICK_USAGE);
			return;
		}
		if (getKickMessageFromCommand(cmd).length() > 25) {
			gui.displayMessage(ServerMessageType.ERROR, "Reason cant be longer than 25 Characters");
			return;
		}
		if (gui.getJTalkerServer().isRunning()) {
			if (server.getClientByName(cmd[1]) != null) {
				server.kickClient(server.getClientByName(cmd[1]), getKickMessageFromCommand(cmd));
			} else {
				gui.displayMessage(ServerMessageType.ERROR, "Client " + cmd[1] + " not online");
			}
		}
	}

	/**
	 * Concatenate all strings after cmd position 2
	 * 
	 * @param cmd
	 * @return Message of the command
	 */
	public String getKickMessageFromCommand(String[] cmd) {
		String message = "";
		for (int counter = 2; counter < cmd.length; counter++) {
			message += " " + cmd[counter];
		}
		return message;
	}

}
