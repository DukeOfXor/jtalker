package main.java.org.bitbucket.dukeofxor.jtalker.launch;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import main.java.org.bitbucket.dukeofxor.jtalker.client.gui.ClientGUI;
import main.java.org.bitbucket.dukeofxor.jtalker.server.gui.ServerUI;

public class LauncherGUI extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		SplitPane rootPane = new SplitPane();

		Scene scene = new Scene(rootPane, 500, 300);
		scene.getStylesheets().add(getClass()
				.getResource("/main/resources/org/bitbucket/dukeofxor/jtalker/gui/styles.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("JTalker");
		primaryStage.setMinWidth(500);
		primaryStage.setMinHeight(300);

		Label clientLabel = new Label("Client");
		clientLabel.getStyleClass().add("title");
		Label serverLabel = new Label("Server");
		serverLabel.getStyleClass().add("title");

		BorderPane leftPane = new BorderPane();
		leftPane.getStyleClass().add("border-pane");
		leftPane.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				ClientGUI clientGUI = new ClientGUI();
				try {
					clientGUI.start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				;
				primaryStage.close();
			}
		});
		leftPane.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				leftPane.getStyleClass().add("highlight");
				clientLabel.getStyleClass().add("highlight");
			}
		});
		leftPane.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				leftPane.getStyleClass().removeAll("highlight");
				clientLabel.getStyleClass().removeAll("highlight");
			}
		});

		BorderPane rightPane = new BorderPane();
		rightPane.getStyleClass().add("border-pane");
		rightPane.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				ServerUI serverUI = new ServerUI();
				try {
					serverUI.start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				;
				primaryStage.close();
			}
		});
		rightPane.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				rightPane.getStyleClass().add("highlight");
				serverLabel.getStyleClass().add("highlight");
			}
		});
		rightPane.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				rightPane.getStyleClass().removeAll("highlight");
				serverLabel.getStyleClass().removeAll("highlight");
			}
		});

		rootPane.getItems().add(leftPane);
		rootPane.getItems().add(rightPane);
		leftPane.setCenter(clientLabel);
		rightPane.setCenter(serverLabel);

		primaryStage.show();
	}

	public static void startup() {
		String[] args = null;
		launch(args);
	}

}
