
package main.java.org.bitbucket.dukeofxor.jtalker.client;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Platform;
import main.java.org.bitbucket.dukeofxor.jtalker.client.gui.ClientGUI;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.ClientListServerMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.ClientLogoutMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.ErrorServerMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.TextServerMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.servertoclient.WhisperServerMessage;

public class MessageListener extends Thread {

  private Client client;
  private boolean running;
  private ClientGUI gui;

  /**
   * @param client
   * @param gui
   */
  public MessageListener(Client client, ClientGUI gui) {
    this.client = client;
    this.gui = gui;
  }

  @Override
  public void run() {
    running = true;
    while (running) {
      try {
        Object receivedObject = client.getInputStream().readObject();

        // TextMessage
        if (receivedObject.getClass().equals(TextServerMessage.class)) {
          TextServerMessage textMessage = (TextServerMessage)receivedObject;

          displayGuiMessage(textMessage.getUsername(), textMessage.getText());
          continue;
        }

        // WhisperMessage
        if (receivedObject.getClass().equals(WhisperServerMessage.class)) {
          WhisperServerMessage whisperMessage = (WhisperServerMessage)receivedObject;

          displayGuiWhisperMessage(whisperMessage.getText(), whisperMessage.getHostUsername(), whisperMessage.getTargetUsername());
        }

        // ErrorMessage
        if (receivedObject.getClass().equals(ErrorServerMessage.class)) {
          ErrorServerMessage errorMessage = (ErrorServerMessage)receivedObject;

          displayGuiErrorMessage(errorMessage.getErrorMessage());
        }

        // ClientlistMessage
        if (receivedObject.getClass().equals(ClientListServerMessage.class)) {
          ClientListServerMessage clientListMessage = (ClientListServerMessage)receivedObject;

          displayGuiClientlist(clientListMessage.getClientlist());
        }

        //ClientLogoutMessage
        if(receivedObject.getClass().equals(ClientLogoutMessage.class)){
        	ClientLogoutMessage clientLogoutMessage = (ClientLogoutMessage)receivedObject;
        	
        	if(clientLogoutMessage.getLogoutState().equals(LogoutState.KICK)){
        		displayClientLogoutMessage("Kicked: " + clientLogoutMessage.getMessage());
        		continue;
        	}
        	if(clientLogoutMessage.getLogoutState().equals(LogoutState.DUPLICATE_USERNAME)){
        		displayClientLogoutMessage("Username " + clientLogoutMessage.getMessage() + " already taken");
        		continue;
        	}
        	displayClientLogoutMessage(clientLogoutMessage.getMessage());
        	
        }

      } catch (ClassNotFoundException e) {
        // can't do anything if class is not found
      } catch (IOException e) {
        // server has closed the connection
        startGuiLoginView("Connection to server lost");
        break;
      }
    }
  }

  /**
   * @param errorMessage
   */
  private void displayGuiErrorMessage(String errorMessage) {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.displayErrorMessage(errorMessage);
      }
    });
  }

  /**
   * @param text
   * @param hostUsername
   * @param targetUsername
   */
  private void displayGuiWhisperMessage(String text, String hostUsername, String targetUsername) {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.displayWhisperMessage(targetUsername, hostUsername, text);
      }
    });
  }

  private void displayClientLogoutMessage(String message) {
    gui.setLogoutMessage(message);
  }

  /**
   * Stop listening to messages
   */
  public void shutdown() {
    running = false;
  }

  private void startGuiLoginView(String reason) {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.startLoginView();
        gui.setLoginErrorText(reason);
      }
    });
  }

  /**
   * Display a new clientList on the GUI
   * 
   * @param clientList
   */
  private void displayGuiClientlist(ArrayList<String> clientList) {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.setClientList(clientList);
      }
    });
  }

  /**
   * Display a chat message from a specific username on the GUI
   * 
   * @param username
   * @param message
   */
  private void displayGuiMessage(String username, String message) {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.displayMessage(username, message);
      }
    });
  }
}
