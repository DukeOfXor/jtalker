package main.java.org.bitbucket.dukeofxor.jtalker.client.gui;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ColorGenerator {

	/**
	 * 
	 * @param input
	 *            Any String
	 * @return A unique hex color with the format #XXXXXX
	 */

	public String generate(String input) {
		// generate hash from input
		String hexString = getHash(input);
		// take last 6 chars from hash and add a '#' to the front
		return "#" + hexString.substring(hexString.length() - 6, hexString.length());
	}

	private String getHash(String input) {
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.reset();
			m.update(input.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1, digest);
			String hashtext = bigInt.toString(16);
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

}
