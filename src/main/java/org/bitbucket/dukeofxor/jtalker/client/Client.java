
package main.java.org.bitbucket.dukeofxor.jtalker.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import javafx.application.Platform;
import main.java.org.bitbucket.dukeofxor.jtalker.client.gui.ClientGUI;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver.LoginClientMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver.LogoutClientMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver.TextClientMessage;
import main.java.org.bitbucket.dukeofxor.jtalker.common.message.clienttoserver.WhisperClientMessage;

public class Client extends Thread {

  private String ip;
  private int port;
  private String username;
  private Socket socket;
  private ObjectInputStream inputStream;
  private ObjectOutputStream outputStream;
  private ClientGUI gui;
  private MessageListener messageListener;

  /**
   * @param ip
   * @param port
   * @param username
   * @param gui
   */
  public Client(String ip, int port, String username, ClientGUI gui) {
    this.ip = ip;
    this.port = port;
    this.username = username;
    this.gui = gui;
  }

  @Override
  public void run() {
    try {
      socket = new Socket(ip, port);
    } catch (Exception e) {
      displayGuiLoginErrorMessage("Connection failed");
      return;
    }

    try {
      setInputStream(new ObjectInputStream(socket.getInputStream()));
      setOutputStream(new ObjectOutputStream(socket.getOutputStream()));
    } catch (IOException e) {
      displayGuiLoginErrorMessage("Connection failed");
      return;
    }

    messageListener = new MessageListener(this, gui);
    messageListener.start();

    try {
      getOutputStream().writeObject(new LoginClientMessage(username));
    } catch (IOException e) {
      e.printStackTrace();
      disconnect();
      displayGuiLoginErrorMessage("Login failed");
      return;
    }

    startGuiChatView();
    resetReasonLabelText();
  }

  private void resetReasonLabelText() {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.setLogoutMessage("");
      }
    });

  }

  /**
   * Log off from the server (needs to be done for a proper disconnect)
   */
  public void logout() {
    try {
      if (getOutputStream() != null) {
        if (!socket.isClosed() && socket.isConnected()) {
          getOutputStream().writeObject(new LogoutClientMessage());
        }
      }
    } catch (SocketException e) {
      // This will throw if the server closes the connection
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Disconnect from the server (you should call logout() first)
   */
  public void disconnect() {
    // ignoring these exceptions, there is not much i can do here
    try {
      if (getInputStream() != null) {
        getInputStream().close();
      }
    } catch (IOException e) {
    }

    try {
      if (getOutputStream() != null) {
        getOutputStream().close();
      }
    } catch (IOException e) {
    }

    try {
      if (socket != null) {
        socket.close();
      }
    } catch (IOException e) {
    }

    messageListener.shutdown();

    startGuiLoginView("Connection to server lost");
  }

  private void displayGuiLoginErrorMessage(String errorMessage) {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.setLoginErrorText(errorMessage);
      }
    });
  }

  /**
   * Change to the chat view of the GUI
   */
  private void startGuiChatView() {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.startChatView();
      }
    });
  }

  /**
   * Change to the login view and sets an error message with the reason
   * 
   * @param reason
   */
  private void startGuiLoginView(String reason) {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        gui.startLoginView();
        gui.setLoginErrorText(reason);
      }
    });
  }

  public ObjectInputStream getInputStream() {
    return inputStream;
  }

  private void setInputStream(ObjectInputStream inputStream) {
    this.inputStream = inputStream;
  }

  public ObjectOutputStream getOutputStream() {
    return outputStream;
  }

  private void setOutputStream(ObjectOutputStream outputStream) {
    this.outputStream = outputStream;
  }

  public String getUsername() {
    return this.username;
  }

  public String getServerIp() {
    return socket.getInetAddress().toString().replace("/", "");
  }

  /**
   * Send a text message to the server
   * 
   * @param text
   */
  public void sendTextMessage(String text) {
    try {
      getOutputStream().writeObject(new TextClientMessage(text));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Send a whisper message to only one client
   * 
   * @param message , clientName
   */
  public void sendWhisperMessage(String message, String targetUsername, String hostUsername) {
    try {
      getOutputStream().writeObject(new WhisperClientMessage(message, targetUsername, hostUsername));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
