
package main.java.org.bitbucket.dukeofxor.jtalker.client.gui;

import main.java.org.bitbucket.dukeofxor.jtalker.client.Client;

public class CommandHandler {
  public static final String TOO_MANY_ARGUMENTS = "Too many arguments for command ";
  public static final String TOO_FEW_ARGUMENTS = "Too few arguments for command ";
  public static final String WHISPER_USAGE = "/whisper [username] [message]";
  public static final String USAGE = "Usage: ";

  private ClientGUI clientGUI;
  private Client client;

  public CommandHandler(ClientGUI clientGUI, Client client) {
    this.clientGUI = clientGUI;
    this.client = client;
  }

  public void handleCommand(String command) {
    String[] cmd = command.substring(1).trim().split(" ");
    if (cmd[0].equalsIgnoreCase("whisper")) {
    	whisper(cmd);
    }
  }

  private void whisper(String cmd[]) {
	  if(cmd.length < 3){
  		clientGUI.displayErrorMessage(TOO_FEW_ARGUMENTS + cmd[0] + "\n" + USAGE + WHISPER_USAGE);
  		return;
  	}
  	client.sendWhisperMessage(getMessageFromCommand(cmd), cmd[1], client.getUsername());
  	return;
  }

  public String getMessageFromCommand(String[] cmd) {
    String message = "";
    for (int counter = 2; counter < cmd.length; counter++) {
      message += " " + cmd[counter];
    }
    return message;
  }
}
