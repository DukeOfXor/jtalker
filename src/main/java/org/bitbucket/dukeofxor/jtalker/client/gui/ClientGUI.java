
package main.java.org.bitbucket.dukeofxor.jtalker.client.gui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.regex.Pattern;

import javafx.application.Application;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import main.java.org.bitbucket.dukeofxor.jtalker.client.Client;

public class ClientGUI extends Application {

	private BorderPane rootPaneLogin;
	private static final Pattern IP_PATTERN = Pattern.compile("^\\s*(.*?):(\\d+)\\s*$");
	private static final Pattern USERNAME_PATTERN = Pattern.compile("^[a-zA-Z0-9]+$");
	private static final String IP_PLACEHOLDER = "192.168.1.102:2347";
	private static final String USERNAME_PLACEHOLDER = "CoolUser99";
	private static final String COMMAND_EXECUTOR_CHAR = "/";
	public static final String TOO_MANY_ARGUMENTS = "Too many arguments for command ";
	public static final String TOO_FEW_ARGUMENTS = "Too few arguments for command ";
	public static final String WHISPER_USAGE = "/whisper [username] [message]";
	public static final String USAGE = "Usage: ";
	private Label labelAddress;
	private TextField textFieldAddress;
	private Label labelUsername;
	private TextField textFieldUsername;
	private Client client;
	private Label labelError;
	private Label labelReason;
	private Button buttonConnect;
	private StackPane stackPaneButtonWrapper;
	private VBox vBoxLoginForm;
	private TextField textFieldChatInput;
	private BorderPane borderPaneMainChatWrapper;
	private Label labelServerIp;
	private Scene scene;
	private BorderPane rootPaneChat;
	private BorderPane borderPaneLeftSideWrapper;
	private ListView<String> listViewClients;
	private ListProperty<String> listPropertyClients;
	private SplitPane splitPaneChat;
	private ListView<InputMessage> listViewTextOutput;
	private HashMap<String, String> colorClientMap;
	private ColorGenerator colorGenerator;

	@Override
	public void start(Stage primaryStage) throws Exception {
		listPropertyClients = new SimpleListProperty<>();
		colorClientMap = new HashMap<String, String>();
		colorGenerator = new ColorGenerator();

		rootPaneLogin = new BorderPane();
		rootPaneChat = new BorderPane();

		scene = new Scene(rootPaneLogin, 600, 500);
		scene.getStylesheets().add(getClass()
				.getResource("/main/resources/org/bitbucket/dukeofxor/jtalker/gui/styles.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("JTalker Client");
		primaryStage.setMinWidth(600);
		primaryStage.setMinHeight(500);
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				if (client != null) {
					client.logout();
					client.disconnect();
					try {
						client.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		labelAddress = new Label("Address");
		labelAddress.getStyleClass().add("label-login");

		textFieldAddress = new TextField();
		textFieldAddress.getStyleClass().add("text-field-login");
		textFieldAddress.setPromptText(IP_PLACEHOLDER);
		textFieldAddress.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode().equals(KeyCode.ENTER)) {
					validateInputs();
				}
			}
		});

		// login form
		labelUsername = new Label("Username");
		labelUsername.getStyleClass().add("label-login");

		textFieldUsername = new TextField();
		textFieldUsername.getStyleClass().add("text-field-login");
		textFieldUsername.setPromptText(USERNAME_PLACEHOLDER);
		textFieldUsername.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode().equals(KeyCode.ENTER)) {
					validateInputs();
				}
			}
		});

		buttonConnect = new Button("Connect");
		buttonConnect.getStyleClass().add("button-login");
		buttonConnect.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				validateInputs();
			}
		});

		stackPaneButtonWrapper = new StackPane(buttonConnect);
		stackPaneButtonWrapper.getStyleClass().add("stack-pane-login");

		labelError = new Label();
		labelError.getStyleClass().add("label-login-error");

		labelReason = new Label();
		labelReason.getStyleClass().add("label-login-error");

		vBoxLoginForm = new VBox();
		vBoxLoginForm.getStyleClass().add("vbox-login");

		vBoxLoginForm.getChildren().addAll(labelAddress, textFieldAddress, labelUsername, textFieldUsername,
				stackPaneButtonWrapper, labelError, labelReason);
		vBoxLoginForm.setAlignment(Pos.CENTER);

		rootPaneLogin.setCenter(vBoxLoginForm);

		primaryStage.show();

		listViewTextOutput = new ListView<InputMessage>();
		listViewTextOutput.setCellFactory(new Callback<ListView<InputMessage>, ListCell<InputMessage>>() {

			@Override
			public ListCell<InputMessage> call(ListView<InputMessage> param) {
				ListCell<InputMessage> cell = new ListCell<InputMessage>() {
					@Override
					protected void updateItem(InputMessage message, boolean empty) {
						super.updateItem(message, empty);
						if (message == null || empty) {
							setText(null);
							setStyle("");
						} else {
							setStyle("-fx-background-color: " + colorClientMap.get(message.getUsername())
									+ "; -fx-background-radius: 20px; -fx-border-color: black; -fx-border-width: 1px; -fx-border-radius: 20px;");
							setText(message.getMessage());
						}
					}

				};
				return cell;
			}
		});
		listViewTextOutput.setEditable(false);

		textFieldChatInput = new TextField();
		textFieldChatInput.getStyleClass().add("text-field-chat-input");
		textFieldChatInput.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode().equals(KeyCode.ENTER)) {
					sendMessage();
				}
			}
		});

		borderPaneMainChatWrapper = new BorderPane();
		borderPaneMainChatWrapper.setCenter(listViewTextOutput);
		borderPaneMainChatWrapper.setBottom(textFieldChatInput);
		borderPaneMainChatWrapper.getStyleClass().add("border-pane-chat-main");

		labelServerIp = new Label();
		labelServerIp.getStyleClass().add("label-chat-server-ip");

		StackPane stackPaneLabelIpWrapper = new StackPane(labelServerIp);
		stackPaneLabelIpWrapper.getStyleClass().add("stack-pane-label-ip-wrapper");

		listViewClients = new ListView<String>();
		listViewClients.itemsProperty().bind(listPropertyClients);

		borderPaneLeftSideWrapper = new BorderPane();
		borderPaneLeftSideWrapper.setPrefWidth(150);
		borderPaneLeftSideWrapper.setMinWidth(150);
		borderPaneLeftSideWrapper.setTop(stackPaneLabelIpWrapper);
		borderPaneLeftSideWrapper.setCenter(listViewClients);
		borderPaneLeftSideWrapper.getStyleClass().add("border-pane-chat-left-side-wrapper");

		splitPaneChat = new SplitPane();
		splitPaneChat.getItems().add(borderPaneLeftSideWrapper);
		splitPaneChat.setDividerPosition(0, 0.25);
		splitPaneChat.getItems().add(borderPaneMainChatWrapper);

		primaryStage.maximizedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				splitPaneChat.setDividerPosition(0, 0.25);
			}
		});

		rootPaneChat.setCenter(splitPaneChat);

		listViewClients.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {

			@Override
			public ListCell<String> call(ListView<String> param) {
				ListCell<String> cell = new ListCell<String>() {
					@Override
					protected void updateItem(String username, boolean empty) {
						super.updateItem(username, empty);
						if (username == null || empty) {
							setText(null);
							setId("");
						} else {
							if (username.equals(client.getUsername())) {
								setId("own-name");
							} else {
								setId("");
							}
							setText(username);
						}
					}
				};
				return cell;
			}
		});

	}

	/**
	 * Change to the chat view of the GUI
	 */
	public void startChatView() {
		labelServerIp.setText(client.getServerIp());
		scene.setRoot(rootPaneChat);
	}

	/**
	 * Change to the login view of the GUI
	 */
	public void startLoginView() {
		scene.setRoot(rootPaneLogin);
	}

	/**
	 * Set the list of clients to display
	 * 
	 * @param clientList
	 */
	public void setClientList(ArrayList<String> clientList) {
		int counter = 0;
		for (String name : clientList) {
			if (counter > clientList.size()) {
				counter = 0;
			}
			colorClientMap.put(name, colorGenerator.generate(name));
			counter++;
		}
		clientList.sort(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});

		listPropertyClients.set(FXCollections.observableArrayList(clientList));
	}

	/**
	 * Display a chat message from a specific username
	 * 
	 * @param username
	 * @param message
	 */
	public void displayMessage(String username, String message) {
		listViewTextOutput.getItems().add(new InputMessage(username, "[" + username + "] " + message + "\n"));
		listViewTextOutput.scrollTo(listViewTextOutput.getItems().size());
	}

	/**
	 * @param hostUsername
	 * @param message
	 * @param targetUsername
	 */
	public void displayWhisperMessage(String targetUsername, String hostUsername, String message) {
		listViewTextOutput.getItems().add(
				new InputMessage(hostUsername, "[" + hostUsername + "]->[" + targetUsername + "] " + message + "\n"));
	}

	public void displayErrorMessage(String message) {
		listViewTextOutput.getItems().add(new InputMessage(null, message + "\n"));
	}

	protected void connect(String ip, String username) {
		String[] split = ip.split(":");
		String address = split[0];
		int port = 8954;
		if (split.length == 2) {
			Integer.parseInt(split[1]);
		}

		client = new Client(address, port, username, this);
		client.start();
	}

	protected boolean isValidUsername(String username) {
		return USERNAME_PATTERN.matcher(username).matches();
	}

	protected boolean isValidIp(String ip) {
		return IP_PATTERN.matcher(ip).matches();
	}

	private void validateInputs() {
		String ip = textFieldAddress.getText();
		String username = textFieldUsername.getText();

		setLoginErrorText("");

		if (isValidIp(ip)) {
			textFieldAddress.getStyleClass().removeAll("text-field-login-invalid");
		} else {
			textFieldAddress.getStyleClass().add("text-field-login-invalid");
		}

		if (isValidUsername(username)) {
			textFieldUsername.getStyleClass().removeAll("text-field-login-invalid");
		} else {
			textFieldUsername.getStyleClass().add("text-field-login-invalid");
		}

		if (isValidIp(ip) && isValidUsername(username)) {
			connect(ip, username);
		}
	}

	/**
	 * Set a error message in the login view
	 * 
	 * @param errorMessage
	 */
	public void setLoginErrorText(String errorMessage) {
		labelError.setText(errorMessage);
	}

	private void sendMessage() {
		String message = textFieldChatInput.getText().trim();
		if (message.isEmpty()) {
			return;
		}
		if (client == null) {
			return;
		}
		if (message.startsWith(COMMAND_EXECUTOR_CHAR)) {
			handleCommand(message);
			textFieldChatInput.clear();
			return;
		}
		client.sendTextMessage(textFieldChatInput.getText());
		textFieldChatInput.clear();
	}

	// Handle command here because JFX have a problem if we handle commands in
	// Other class
	public void handleCommand(String command) {
		String[] cmd = command.substring(1).trim().split(" ");
		if (cmd[0].equalsIgnoreCase("whisper")) {
			whisper(cmd);
		}
	}

	private void whisper(String cmd[]) {
		if (cmd.length < 3) {
			displayErrorMessage(TOO_FEW_ARGUMENTS + cmd[0] + "\n" + USAGE + WHISPER_USAGE);
			return;
		}
		client.sendWhisperMessage(getMessageFromCommand(cmd), cmd[1], client.getUsername());
		return;
	}

	/**
	 * Set message on login window
	 * 
	 * @param message
	 */
	public void setLogoutMessage(String message) {
		labelReason.setText(message);
	}

	public String getMessageFromCommand(String[] cmd) {
		String message = "";
		for (int counter = 2; counter < cmd.length; counter++) {
			message += " " + cmd[counter];
		}
		return message;

	}

}